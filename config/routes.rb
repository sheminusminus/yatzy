Rails.application.routes.draw do
  root to: 'rooms#index'
  get 'test', to: 'test#index'
  get 'rooms', to: 'rooms#index'
  get 'rooms/:id', to: 'rooms#show', as: 'roomId'
  get 'rooms/new', to: 'rooms#new'
  get 'games', to: 'games#index'
  get 'games/:id', to: 'rooms#show'
  get 'games/new', to: 'games#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
