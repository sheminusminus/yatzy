# yatzy 

yahtzee on steroids with 6 dice.

## install it 

(requires ruby and yarn)

```
git clone https://gitlab.com/sheminusminus/yatzy
cd yatzy
bundle install
yarn install
```

## run it

```
npm run fm
```

this will start foreman running your server on port 5000.

webpack will take a sec to catch up, but the js will compile after a few moments. the react src files are in the /client directory (which is output to the /app/assets directory when compiled).

## the rails stuff

rails' responsibilities:

- manning the server and routing
- handling api calls
- management of rooms, games, and player connections
- dispatch updates to clients

personal rails goals (so far):

- write a decent api
- pull off the multiplayer update-stream with action cables (rails' web sockets)

### talking to the app-level react component from rails

rails controllers acquire new data, which is handed off to a view, and injected into the main component (as props).

## the react stuff

react's responsibilities:

- draw pretty things onto the screen (render update stream)
- handle immediate user actions and input
- alert the server api when relevant things are happening

personal react goals (so far):

- as DRY as possible
- make middleman handlers for repeated code (e.g. calls to preventDefault)
- views and interaction only; no writing app controllers in the react code