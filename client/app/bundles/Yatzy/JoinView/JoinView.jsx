import React from 'react';
import Header from '../Header/Header';
import JoinForm from './JoinForm';

export default class JoinView extends React.Component {
    constructor(props, _railsContext) {
        super(props);
    }

    render() {
        return (
            <div>
                <JoinForm
                    roomId={this.props.roomId}
                    playerName={this.props.playerName}
                    handleName={this.props.handleName}
                    joinRoom={this.props.joinRoom} />
            </div>
        );
    }
}