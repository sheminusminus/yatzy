import React from 'react';

export default class JoinForm extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <form>
                <input
                    type="text"
                    placeholder="your display name"
                    value={this.props.playerName}
                    onChange={this.props.handleName} />
                <br/>
                <button onClick={this.props.joinRoom}>Join</button>
            </form>
        );
    };

}