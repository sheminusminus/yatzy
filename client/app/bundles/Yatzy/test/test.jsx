import React from 'react';
import { createStore } from 'redux';

import { setView, Views } from '../constants/nav-actions';
import { toggleHoldDie, rollDice } from '../constants/player-actions';

import yatzy from '../reducers/root';

export default class Test extends React.Component {
    constructor(props) {
        super(props);
        this.state = props;
        this.runTest = this.runTest.bind(this);
        this.store = createStore(yatzy, props);
        this.unsubscribe = this.store.subscribe(() =>
            console.log(this.store.getState())
        );
    }

    roll() {
        return Math.floor(Math.random() * 6 + 1);
    }

    runTest() {
        console.log(this.store.getState());
        this.store.dispatch(setView(Views.INDEX_ROOMS));
        this.store.dispatch(setView(Views.JOIN_ROOM));
        this.store.dispatch(setView(Views.CREATE_ROOM));
        var testDice = ([this.roll(), this.roll(), this.roll(), this.roll(), this.roll(), this.roll()]).map(function(val, idx) {
            return {
                value: val,
                index: idx,
                isHeld: false
            };
        });
        this.store.dispatch(rollDice(testDice));
        this.store.dispatch(toggleHoldDie(0));
        this.store.dispatch(toggleHoldDie(3));
        this.unsubscribe();
    }

    render() {
        return (
            <div>
                <button onClick={this.runTest}>test</button>
            </div>
        );
    }
}

