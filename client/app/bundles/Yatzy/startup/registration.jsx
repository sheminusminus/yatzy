import ReactOnRails from 'react-on-rails';

// application component --> main parent
import App from '../index';

// this lets reactonrails see the app component in browser
ReactOnRails.register({
  App,
});
