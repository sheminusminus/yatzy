export const ROLL_DICE = 'ROLL_DICE';
export const TOGGLE_HOLD_DIE = 'TOGGLE_HOLD_DIE';
export const SCORE_ROUND = 'SCORE_ROUND';

export function scoreRound(diceVals) {
    return { type: SCORE_ROUND, payload: diceVals };
}

export function toggleHoldDie(dieIndex) {
    return { type: TOGGLE_HOLD_DIE, payload: dieIndex };
}

export function rollDice(diceVals) {
    return { type: ROLL_DICE, payload: diceVals };
}

