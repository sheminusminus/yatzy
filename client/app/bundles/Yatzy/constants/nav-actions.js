/**
 * Created by emilykolar on 4/19/17.
 */
const INDEX_ROOMS = 'INDEX_ROOMS';
const CREATE_ROOM = 'CREATE_ROOM';
const JOIN_ROOM = 'JOIN_ROOM';

export const SET_VIEW = 'SET_VIEW';

export const Views = {
    INDEX_ROOMS,
    CREATE_ROOM,
    JOIN_ROOM,
};

export function setView(nextView) {
    return { type: SET_VIEW, payload: nextView };
}