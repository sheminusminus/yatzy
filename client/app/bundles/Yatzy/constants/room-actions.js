
export const SET_NAME = 'SET_NAME';
export const FIND_BY_NAME = 'FIND_BY_NAME';
export const SELECT_ROOM = 'SELECT_ROOM';

export function setName(roomName) {
	return { type: SET_NAME, payload: roomName };
}

export function findByName(roomName) {
	return { type: FIND_BY_NAME, payload: roomName };
}

export function selectRoom(roomId) {
	return { type: SELECT_ROOM, payload: roomId };
}