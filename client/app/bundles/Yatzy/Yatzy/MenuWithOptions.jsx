import { connect } from 'react-redux'

import { setView } from '../constants/nav-actions'

import AppMenu from '../AppMenu/AppMenu';

import IndexView from '../IndexView/IndexView';
import CreateView from '../CreateView/CreateView';
import JoinView from '../JoinView/JoinView';

const getMenuOptionsData = (appMenu, view) => {
	return appMenu.map(function(opt, index) {
		return {
			text: opt.text,
			forView: opt.forView,
			isActive: (opt.forView === view)
		}
	});
};

const mapStateToProps = (state) => {
	return {
		appMenu: getMenuOptionsData(state.appMenu, state.view)
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		onOptionClick: (view) => {
			dispatch(setView(view))
		}
	}
};

const MenuWithOptions = connect(
	mapStateToProps,
	mapDispatchToProps
)(AppMenu);

export default MenuWithOptions;
