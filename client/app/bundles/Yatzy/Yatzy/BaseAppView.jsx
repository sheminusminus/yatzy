import React from 'react';
import PropTypes from 'prop-types';

import { Views } from '../constants/nav-actions';

import IndexView from '../IndexView/IndexView';
import CreateView from '../CreateView/CreateView';
import JoinView from '../JoinView/JoinView';

const BaseAppView = function({view}) {
	switch (view) {
		case Views.INDEX_ROOMS:
			return <IndexView/>;
		case Views.CREATE_ROOM:
			return <CreateView/>;
		case Views.JOIN_ROOM:
			return <JoinView/>;
	}
};

BaseAppView.propTypes = {
	view: PropTypes.string.isRequired
};

export default BaseAppView;