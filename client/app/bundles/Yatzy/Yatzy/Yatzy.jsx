import React from 'react';

import Header from '../Header/Header';
import MenuWithOptions from './MenuWithOptions';
import CreateBaseView from './CreateBaseView';

export default class Yatzy extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				<Header title={'Title'} />
				<MenuWithOptions />
				<CreateBaseView />
				footer
			</div>
		);
	}
}