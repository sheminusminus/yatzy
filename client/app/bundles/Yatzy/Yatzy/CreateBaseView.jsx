import { connect } from 'react-redux'

import BaseAppView from './BaseAppView';

const mapStateToProps = (state) => {
	return {
		view: state.view
	}
};

const CreateBaseView = connect(
	mapStateToProps
)(BaseAppView);

export default CreateBaseView;
