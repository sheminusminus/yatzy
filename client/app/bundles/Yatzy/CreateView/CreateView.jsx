import React from 'react';
import CreateForm from './CreateForm';

const CreateView = ({ handleRoomName }) => (
    <div>
        <CreateForm />
    </div>
);

export default CreateView;