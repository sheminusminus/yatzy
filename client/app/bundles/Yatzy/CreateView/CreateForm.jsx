import React from 'react'
import { connect } from 'react-redux'

// TODO: createRoom action-creator and necessary action types
import { setName } from '../constants/room-actions'

let CreateForm = ({ dispatch }) => {
	let input;
	return (
		<form onSubmit={e => {
			e.preventDefault();
			if (!input.value.trim()) { return; }
			dispatch(setName(input.value));
		}}>
			<input type="text"
				   placeholder="room nickname"
				   ref={node => { input = node }}/>
			<button type="submit">Create a Yatzy Room</button>
		</form>
	);
};

CreateForm = connect()(CreateForm);

export default CreateForm;