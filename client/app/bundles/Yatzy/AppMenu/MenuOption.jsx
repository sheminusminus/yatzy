import React from 'react';
import PropTypes from 'prop-types';

const MenuOption = ({ forView, text, isActive, onClick }) => (
	<li className={isActive ? 'menu-option active' : 'menu-option'} onClick={onClick}>
		{ text }
	</li>
);

MenuOption.propTypes = {
	text: PropTypes.string.isRequired,
	forView: PropTypes.string.isRequired,
	onClick: PropTypes.func.isRequired
};

export default MenuOption;