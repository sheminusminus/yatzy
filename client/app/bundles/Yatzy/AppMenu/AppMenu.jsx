import React from 'react';
import PropTypes from 'prop-types';

import MenuOption from './MenuOption';

const AppMenu = ({ appMenu, onOptionClick }) => (
    <ul className="app-menu">
		{appMenu.map(opt =>
            <MenuOption
                key={opt.forView}
				forView={opt.forView}
				text={opt.text}
				isActive={opt.isActive}
                onClick={() => onOptionClick(opt.forView)}
            />
		)}
    </ul>
);

AppMenu.propTypes = {
	appMenu: PropTypes.arrayOf(PropTypes.shape({
		forView: PropTypes.string.isRequired,
		text: PropTypes.string.isRequired
	}).isRequired).isRequired,
	onOptionClick: PropTypes.func.isRequired
}

export default AppMenu;