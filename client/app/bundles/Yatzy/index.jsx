import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import yatzyState from './reducers/root';
import Yatzy from './Yatzy/Yatzy';

export default class App extends React.Component {
	constructor(props, _railsContext) {
		super(props);
		let context = this;
		this.store = createStore(yatzyState, props);
		this.unsubscribe = this.store.subscribe(() => {
			console.log(context.store.getState());
		});
	}
	render() {
		return (
			<Provider store={this.store}>
				<Yatzy />
			</Provider>
		);
	}
}