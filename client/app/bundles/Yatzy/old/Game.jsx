import PropTypes from 'prop-types';
import React from 'react';

import GameTable from './GameTable';
import PlayerSeat from './PlayerSeat';
import Scorecard from './Scorecard';

import { Upper, Lower, UPPER_TOTAL, LOWER_TOTAL, TOTAL } from '../constants/scoring';
import { ROLL_DICE, TOGGLE_HOLD_DIE } from '../constants/player-actions';

export default class Game extends React.Component {

  constructor(props, _railsContext) {
    super(props);
    this.state = { 
		dice: [],
		turnRolls: 3
	};
	this.handleHold = this.handleHold.bind(this);
	this.rollUnheldDice = this.rollUnheldDice.bind(this);
  }
  
  restate(nextState) {
	  console.log('prev:', this.state);
	  console.log('next:', nextState);
	  this.setState(nextState);
  }
  
  componentDidMount() {
	  //this.rollUnheldDice();
  }
  
  handleHold(idx) {
	  var dice = this.state.dice;
	  dice[idx].isHeld = !(dice[idx].isHeld);
	  const nextState = {
		  dice: dice,
		  turnRolls: this.state.turnRolls
	  };
	  this.restate(nextState);
  }
  
  rollUnheldDice() {
	  const dice = this.rollDice();
	  const nextState = {
	    dice: dice,
		turnRolls: this.state.turnRolls - 1
	  };
	  this.restate(nextState);
  }
  
  rollDice() {
	  var dice = [];
	  for (var i = 0; i < 6; i++) {
		  if (this.state.dice[i] && this.state.dice[i].isHeld) { 
			  dice.push(this.state.dice[i]); 
			  continue; 
		  }
		  dice.push({value: this.rollDie(), index: i, isHeld: false});
	  }
	  return dice;
  }
  
  rollDie = () => {
	return Math.floor(Math.random() * 6 + 1); 
  };
  
  render() {
    return (
      <div className="game">
	  	<GameTable 
	  		dice={this.state.dice} 
	  		turnRolls={this.state.turnRolls} 
	  		handleRoll={this.rollUnheldDice} 
	  		handleHold={this.handleHold} />
	  		<Scorecard />
      </div>
    );
  }
}