import React from 'react';

export default class Footer extends React.Component {
	
  constructor(props, _railsContext) {
	  super(props);
  }	
	
  render() {
    return (
      <footer className="footer">
	  	<p>Emily Kolar | {(new Date()).getFullYear()}</p>
      </footer>
    );
  }
}

//<p>Handmade in Chicago | Emily Kolar | {(new Date()).getFullYear()}</p>