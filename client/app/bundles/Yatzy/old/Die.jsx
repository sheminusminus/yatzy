import PropTypes from 'prop-types';
import React from 'react';

export default class Die extends React.Component {
  static propTypes = {
    value: PropTypes.number.isRequired, // this is passed from the rails view
  };

  /**
   * @param props - Comes from your rails view.
   * @param _railsContext - Comes from React on Rails
   */
  constructor(props, _railsContext) {
    super(props);
    this.handleHold = this.handleHold.bind(this);
  }
  
  handleHold(evt) {
  	evt.preventDefault();
  	this.props.handleHold(this.props.index);
  }

  render() {
	const classes = this.props.isHeld ? 'die held' : 'die';
	const dieImage = `assets/images/d${this.props.value}.png`;
    return (
      <div 
      	className={classes} 
      	style={{backgroundImage: `url(${dieImage})`}} 
      	onClick={this.handleHold}>
	    {this.props.value}
      </div>
    );
  }
}