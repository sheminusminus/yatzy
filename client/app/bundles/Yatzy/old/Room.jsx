import PropTypes from 'prop-types';
import React from 'react';

import Game from './Game';
import Player from './Player';

export default class Room extends React.Component {
  /**
   * @param props - Comes from your rails view.
   * @param _railsContext - Comes from React on Rails
   */
  constructor(props, _railsContext) {
    super(props);
    this.state = { 
	    players: [],
	    playersVictories: []    
	};
  }
  
  addPlayer = (player) => {
	  var players = this.state.players;
	  players.push(player);
	  this.setState({ players });
  }

  render() {
    return (
      <div className="room">
	    <Game />
	    <Player />
      </div>
    );
  }
}
