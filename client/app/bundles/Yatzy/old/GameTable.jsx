import PropTypes from 'prop-types';
import React from 'react';

import Die from './Die';

export default class GameTable extends React.Component {

  constructor(props, _railsContext) {
    super(props);
  }
  
  renderDice() {
	  console.log(this.props.dice);
	  var context = this;
	  var dice = this.props.dice.map(function(die, idx) {
		  return (
		  	<Die 
			  	value={die.value} 
			  	key={`die-${idx}`} 
			  	isHeld={die.isHeld} 
			  	index={idx} 
			  	handleHold={context.props.handleHold} />
	      );
	  });
	  return dice;
  }
  
  renderRollButton = () => {
	  const disabled = (this.props.turnRolls <= 0);
	  return (
	  	<button 
	  		className="roll-button" 
	  		disabled={disabled} 
	  		onClick={this.props.handleRoll}>
	  			{this.props.turnRolls.toString()} Rolls
	    </button>
	  );
  };

  render() {
    const dice = this.renderDice();
    const rollButton = this.renderRollButton();
    return (
      <div className="game-table">
      	<div className="dice-wrapper">
		    {dice}
		</div>
		{rollButton}
      </div>
    );
  }
}