import React from 'react';

export default class Scorecard extends React.Component {
  /**
   * @param props - Comes from your rails view.
   * @param _railsContext - Comes from React on Rails
   */
  constructor(props, _railsContext) {
    super(props);
    this.state = { playerId: this.props.playerId };
  }

  render() {
    return (
      <div>
	    {this.state.playerId}
      </div>
    );
  }
}