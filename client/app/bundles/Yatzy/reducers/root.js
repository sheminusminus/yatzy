import {
    ROLL_DICE,
    TOGGLE_HOLD_DIE
} from '../constants/player-actions';

import {
    SET_VIEW,
    Views
} from '../constants/nav-actions';

import {
    SET_NAME,
    FIND_BY_NAME
} from '../constants/room-actions';

const initialState = {
    view: Views.INDEX_ROOMS,
    title: '',
    dice: [],
    playerName: '',
    activePlayer: '',
    playerId: '',
    activeRoom: {
      name: '',
      id: 0
    },
    upperScores: {},
    lowerScores: {},
    upperTotal: 0,
    lowerTotal: 0,
    appMenu: [
        {forView: Views.INDEX_ROOMS, text: 'Home'},
        {forView: Views.CREATE_ROOM, text: 'Create'},
        {forView: Views.JOIN_ROOM, text: 'Join'}
    ]
};

const { INDEX_ROOMS } = Views;

const { activeRoom } = initialState;

function dice(state = [], action) {
    switch (action.type) {
        case ROLL_DICE:
            return action.payload;
        case TOGGLE_HOLD_DIE:
            return state.map((die, index) => {
                if (index === action.payload) {
                    return Object.assign({}, die, {
                        isHeld: !(die.isHeld)
                    })
                }
                return die;
            });
        default:
            return state;
    }
};

function view(state = INDEX_ROOMS, action) {
    switch (action.type) {
        case SET_VIEW:
            return action.payload;
        default:
            return state;
    }
}

function room(state = activeRoom, action) {
    switch (action.type) {
        case SET_NAME:
            return { name: action.payload, id: state.id };
        case FIND_BY_NAME:
            return { name: action.payload, id: 0 };
    }
}

function yatzyState(state = initialState, action) {
    return {
        ...state,
        view: view(state.view, action),
        dice: dice(state.dice, action),
        activeRoom: room(state.activeRoom, action)
    };
};

export default yatzyState;