import React from 'react';
import PropTypes from 'prop-types';

const Header = ({ title }) => (
	<header className="header">
		<h1 className="h1 name-of-the-game">{'6-Dice Yatzy'}</h1>
		<h3>{title}</h3>
	</header>
);

Header.propTypes = {
	title: PropTypes.string.isRequired
};

export default Header;