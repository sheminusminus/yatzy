class Scores {
		
	constructor() {
		this.ones = 0;
		this.twos = 0;
		this.threes = 0;
		this.fours = 0;
		this.fives = 0;
		this.sixes = 0;
		this.subtotal = 0;
		this.over84Bonus = 0;
		this.upperTotal = 0;
		this.onePair = 0;
		this.twoPair = 0;
		this.threePair = 0;
		this.threeKind = 0;
		this.fourKind = 0;
		this.fiveKind = 0;
		this.smallSt = 0;
		this.largeSt = 0;
		this.royalSt = 0;
		this.cabin = 0;
		this.tower = 0;
		this.chance = 0;
		this.yatzy = 0;
		this.bonusYatzy = 0;
		this.lowerTotal = 0;
	}
	
	get score() {
		return this.calcScore();
	}
	
	get upperScore() {
		return this.calcUpperScore();
	}
	
	get lowerScore() {
		return this.calcLowerScore();
	}
	
	calcUpperScore() {
		return this.ones + this.twos + this.threes + this.fours + 
		this.fives + this.sixes + this.over84Bonus;
	}
	
	calcLowerScore() {
		return this.onePair + this.twoPair + this.threePair + 
		this.threeKind + this.fourKind + this.fiveKind + this.smallSt + 
		this.largeSt + this.royalSt + this.cabin + this.tower + 
		this.chance + this.yatzy + this.bonusYatzy;
	}
	
	calcScore() {
		return this.calcUpperScore() + this.calcLowerScore();
	}
	
}