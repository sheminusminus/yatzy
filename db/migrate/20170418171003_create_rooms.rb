class CreateRooms < ActiveRecord::Migration[5.0]
  def change
    create_table :rooms do |t|
      t.integer :seats_available

      t.timestamps
    end
  end
end
