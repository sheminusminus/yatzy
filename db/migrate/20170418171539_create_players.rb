class CreatePlayers < ActiveRecord::Migration[5.0]
  def change
    create_table :players do |t|
      t.string :username
      t.references :room, foreign_key: true
      t.integer :score
      t.string :uid

      t.timestamps
    end
  end
end
