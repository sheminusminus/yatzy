class CreateGames < ActiveRecord::Migration[5.0]
  def change
    create_table :games do |t|
      t.integer :score
      t.references :room, foreign_key: true
      t.boolean :started
      t.boolean :completed

      t.timestamps
    end
  end
end
