class AddRoundNumToGames < ActiveRecord::Migration[5.0]
  def change
    add_column :games, :round_num, :integer
  end
end
