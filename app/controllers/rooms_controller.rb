class RoomsController < ApplicationController
  # when you just showed up:
  def index
	# create an object used to pass in props to our react app component
    @app_props = {
        view: 'INDEX_ROOMS',
        title: 'All Rooms',
        dice: [],
        playerName: 'player',
        activePlayer: '',
        playerId: '1',
        activeRoom: {
            name: '',
            id: 0
        },
        upperScores: {},
        lowerScores: {},
        upperTotal: 0,
        lowerTotal: 0,
        appMenu: [
            {forView: 'INDEX_ROOMS', text: 'Home'},
            {forView: 'CREATE_ROOM', text: 'Create'},
            {forView: 'JOIN_ROOM', text: 'Join'}
        ]
    }
  end
  # create a room:
  def new
    @app_props = {
        view: 'CREATE_ROOM',
        title: 'Create a Room',
        dice: [],
        playerName: 'player',
        activePlayer: '',
        playerId: '1',
        activeRoom: {
            name: '',
            id: 0
        },
        upperScores: {},
        lowerScores: {},
        upperTotal: 0,
        lowerTotal: 0,
        appMenu: [
            {forView: 'INDEX_ROOMS', text: 'Home'},
            {forView: 'CREATE_ROOM', text: 'Create'},
            {forView: 'JOIN_ROOM', text: 'Join'}
        ]
    }
  end
  # join existing room:
  def show
    @app_props = {
        view: 'JOIN_ROOM',
        title: 'Join a Room',
        dice: [],
        playerName: 'player',
        activePlayer: '',
        playerId: '1',
        activeRoom: {
            name: 'someRoom',
            id: params[:id]
        },
        upperScores: {},
        lowerScores: {},
        upperTotal: 0,
        lowerTotal: 0,
        appMenu: [
            {forView: 'INDEX_ROOMS', text: 'Home'},
            {forView: 'CREATE_ROOM', text: 'Create'},
            {forView: 'JOIN_ROOM', text: 'Join'}
        ]
    }
  end
end