class TestController < ApplicationController
  def index
    @app_props = {
        view: 'INDEX_ROOMS',
        dice: [],
        playerName: 'player',
        activePlayer: '',
        playerId: '1',
        roomId: 0,
        upperScores: {},
        lowerScores: {},
        upperTotal: 0,
        lowerTotal: 0
    }
  end
end
